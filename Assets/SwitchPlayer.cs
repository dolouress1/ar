using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SwitchPlayer : MonoBehaviour
{
public GameObject[] players;
    private int inc = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(inc==players.Length-1)
                inc = 0;
            else
                inc++;
            players[inc].SetActive(true);
            for (int i = 0; i < players.Length; i++)
            {
                if (i == inc)
                    continue;
                players[i].SetActive(false);
            }
        }
        else
        {
            players[inc].SetActive(true);
            for (int i = 0; i < players.Length; i++)
            {
                if (i == inc)
                    continue;
                players[i].SetActive(false);
            }
        }

    }
}
