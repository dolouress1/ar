using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeBackgroundMusic : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        
    }


    [SerializeField]  private AudioSource funky;
    [SerializeField]  private AudioSource pop;
    [SerializeField]  private AudioSource waltz;
    [SerializeField]  private AudioSource rock;
    [SerializeField]  private AudioSource startingBG;

    public void changeToPop(){
        startingBG.Stop();
        rock.Stop();
        funky.Stop();
        waltz.Stop();
        pop.Play();
    }

    public void changeToRock(){
        startingBG.Stop();
        pop.Stop();
        funky.Stop();
        waltz.Stop();
        rock.Play();
    }

    public void changeToWaltz(){
        startingBG.Stop();
        rock.Stop();
        funky.Stop();
        pop.Stop();
        waltz.Play();
    }

    public void changeToFunk(){
        startingBG.Stop();
        pop.Stop();
        funky.Stop();
        waltz.Stop();
        funky.Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
