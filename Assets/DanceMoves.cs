using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DanceMoves : MonoBehaviour
{
    Animator animator;
    private Vector2 startTouchPosition;
    private Vector2 endTouchPosition;

    private void Start(){
        animator = GetComponent<Animator>();
    }

    [SerializeField] private AudioSource source1;

    [SerializeField] private AudioSource source2;

    private void Update()
    {
        if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began){
            startTouchPosition = Input.GetTouch(0).position;
        }
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            endTouchPosition = Input.GetTouch(0).position;
        }
        if (Input.GetKeyDown(KeyCode.RightArrow) || endTouchPosition.x > startTouchPosition.x)
        {
            // Dance move for right swipe
            source1.Play();
            animator.Play("DancePack@Dance06");
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow) || endTouchPosition.x < startTouchPosition.x)
        {
            // Dance move for left swipe
            source1.Play();
            animator.Play("DancePack@Dance07");
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow) || endTouchPosition.y > startTouchPosition.y)
        {
            // Dance move for up swipe
            source2.Play();
            animator.Play("DancePack@Dance03");
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow) || endTouchPosition.y < startTouchPosition.y)
        {
            // Dance move for down swipe
            source2.Play();
            animator.Play("DancePack@Dance04");
        }
    }
     
}