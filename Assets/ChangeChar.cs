using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeChar : MonoBehaviour
{
    public GameObject[] players;
    // Start is called before the first frame update

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        int level = LevelManager.getLevel();
        players[level - 1].SetActive(true);
        for (int i = 0; i < players.Length; i++)
        {
            if (i == level - 1)
                continue;
            players[i].SetActive(false);
        }
    }

}